<?php

namespace think\addons;

use app\common\library\Auth;
use think\facade\Config;
use think\facade\Hook;
use think\facade\Lang;
use think\Loader;
use think\Request;

/**
 * 插件基类控制器
 * @package think\addons
 */
class Controller extends \think\Controller
{

    // 当前插件操作
    protected $addon = null;
    protected $controller = null;
    protected $action = null;
    // 当前template
    protected $template;
    // 模板配置信息
    protected $config = [
        'type'         => 'Think',
        'view_path'    => '',
        'view_suffix'  => 'html',
        'strip_space'  => true,
        'view_depr'    => DS,
        'tpl_begin'    => '{',
        'tpl_end'      => '}',
        'taglib_begin' => '{',
        'taglib_end'   => '}',
    ];

    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = ['*'];

    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $noNeedRight = ['*'];

    /**
     * 权限Auth
     * @var Auth 
     */
    protected $auth = null;
    
    /**
     * 布局模板
     * @var string 
     */
    protected $layout = null;

    /**
             * 架构函数
     * @param Request $request Request对象
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
	}
	
    // 初始化
    public function initialize()
    {
        //移除HTML标签
        $this->request->filter('strip_tags');            
        
        // 是否自动转换控制器和操作名
        $convert = Config::get('url_convert');

        $filter = $convert ? 'strtolower' : 'trim';
        // 处理路由参数
        $param = $this->request->param();
        $dispatch = $this->request->dispatch();
        $var = !empty($dispatch->getParam()) ? $dispatch->getParam() : [];
        $var = array_merge($param, $var);
        if (!empty($dispatch->getDispatch()) && substr($dispatch->getDispatch()[0], 0, 7) == "\\addons")
        {
            $arr = explode("\\", $dispatch->dispatch[0]);
            $addon = strtolower($arr[2]);
            $controller = strtolower(end($arr));
            $action = $dispatch->dispatch[1];
        }
        else
        {
            $addon = isset($var['addon']) ? $var['addon'] : '';
            $controller = isset($var['controller']) ? $var['controller'] : '';
            $action = isset($var['action']) ? $var['action'] : '';
        }
        $this->addon = $addon ? call_user_func($filter, $addon) : '';
        $this->controller = $controller ? call_user_func($filter, $controller) : 'index';
        $this->action = $action ? call_user_func($filter, $action) : 'index';
        // 设置插件模板路径
        $this->view->engine->config('view_path',ADDON_PATH . $this->addon . '\\view\\');
        // 设置插件模板输出替换
        $replace_str =  $this->view->engine->config('tpl_replace_string');
        $this->view->engine->config('tpl_replace_string',array_merge($replace_str,
                [
                    '__ADDON__'=>Config::get('site.cdnurl') . "/assets/addons/" . $this->addon,
                ]
            ));
        // 如果有使用模板布局
        if ($this->layout)
        {
            $this->view->engine->layout('layout/' . $this->layout);
        }        
        
        // 加载系统语言包
        Lang::load( ADDON_PATH . $this->addon . '\\lang\\' .  $this->request->langset() . '.php');

        $this->auth = Auth::instance();
        // token
        $token = $this->request->server('HTTP_TOKEN', $this->request->request('token', \think\facade\Cookie::get('token')));

        $path = 'addons/' . $this->addon . '/' . str_replace('.', '/', $this->controller) . '/' . $this->action;
        // 设置当前请求的URI
        $this->auth->setRequestUri($path);
        // 检测是否需要验证登录
        if (!$this->auth->match($this->noNeedLogin))
        {
            //初始化
            $this->auth->init($token);
            //检测是否登录
            if (!$this->auth->isLogin())
            {
                $this->error(__('Please login first'), 'home/user/login');
            }
            // 判断是否需要验证权限
            if (!$this->auth->match($this->noNeedRight))
            {
                // 判断控制器和方法判断是否有对应权限
                if (!$this->auth->check($path))
                {
                    $this->error(__('You have no permission'));
                }
            }
        }
        else
        {
            // 如果有传递token才验证是否登录状态
            if ($token)
            {
                $this->auth->init($token);
            }
        }
        
        $this->view->assign('user', $this->auth->getUser());

        // 语言检测
        $lang = strip_tags(Lang::detect());
        //上传配置
        $upload = \app\common\model\back\set\Config::upload();//读取系统上传配置
        $upload = Hook::listen("upload_config_init", $upload);//读取插件上传配置
        Config::set('upload', array_merge(Config::get('upload.'), $upload));

        // 站点配置
        $site = Config::get("site.");

        //配置设置：插件前端，借用系统前端目录存放js
        $this->config = [
            'site'           => array_intersect_key($site, array_flip(['name', 'cdnurl', 'version', 'timezone', 'languages'])),
            'upload'         => $upload,
            //'addon'     => $this->addon,
            'modulename'     => $this->addon,
            'controllername' => $this->controller,
            'actionname'     => $this->action,
            'jsname'         => 'homeend/'.$this->addon.'/' . str_replace('.', '/', $this->controller),
            'moduleurl'      => rtrim(url("/home", '', false), '/'),
            'language'       => $lang
        ];
        $this->config = array_merge($this->config, $this->view->engine->config('tpl_replace_string'));
        
        Hook::listen("config_init", $this->config);
        
        // 渲染配置到视图中
        $this->config = array_merge($this->config, get_addon_config($this->addon));
        $this->view->assign("config", $this->config);        
        
        // 加载当前控制器语言包
        $this->assign('site', $site);
        
        //parent::initialize();
    }

    /**
            * 加载模板输出
     * @access protected
     * @param string $template 模板文件名
     * @param array $vars 模板输出变量
     * @param array $config 模板参数
     * @return mixed
     */
    protected function fetch($template = '', $vars = [], $config = [])
    {
        $controller = Loader::parseName($this->controller);
        if ('think' == strtolower(config('template.type')) && $controller && 0 !== strpos($template, '/'))
        {
            $depr = $this->view->engine->config('view_depr');
            $template = str_replace(['/', ':'], $depr, $template);
            if ('' == $template)
            {
                // 如果模板文件名为空 按照默认规则定位
                $template = str_replace('.', $depr, $controller) . $depr . $this->action;
                
            }
            elseif (false === strpos($template, $depr))
            {
                $template = str_replace('.', $depr, $controller) . $depr . $template;
            }
        }
        return parent::fetch($template, $vars, $config);
    }

}
